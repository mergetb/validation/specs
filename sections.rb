#!/usr/bin/ruby

# This file takes an ASCII doc and produces a list of
#
# - section numbers
# - list item numbers
#
# The purpose of this tool is to provide a complete list of sections and list
# items within sections in JSON format as input for tools that calculate
# specification test coverage.
#
# For example
#
#```
# = Doc
# :sectnums:
#
# == Alpha
# === Muffin
# === Pizza
#
# == Omega
# === Ice Cream
# === Mochi
#```
#
# will produce
# ```
# [
#   "1",
#   "1.1",
#   "1.1.1",
#   "1.1.2",
#   "1.2",
#   "1.2.1",
#   "1.2.2",
# ]
#

require 'asciidoctor'
require 'json'

if ARGV.length < 1
  puts "usage: sections.rb <adoc file>"
  exit 1
end

doc = Asciidoctor.load_file ARGV[0]

$result = []

def crawl(sections, parent)
  for s in sections
    if s.title == "Glossary"
      next
    end
    sec = "#{parent}#{s.numeral}"
    #puts sec
    $result<< sec
    crawl(s.sections, "#{sec}.")

    for x in s.blocks
      if x.is_a?(Asciidoctor::List)
        crawlList(x, "#{sec}.")
      end
    end
  end
end

def crawlList(list, parent)
  i = 1
  for block in list.blocks
    sec = "#{parent}#{i}"
    $result << sec
    if block.is_a?(Asciidoctor::List)
      crawlList(x, "#{sec}.")
    end
    i = i+1
  end
end
    

crawl(doc.sections, "")

puts JSON.pretty_generate($result)
