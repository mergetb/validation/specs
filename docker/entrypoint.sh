#!/bin/bash

# Dockerfile entrypoint script
#
# Based on https://denibertovic.com/posts/handling-permissions-with-docker-volumes/ 
#

set -e

# Add local user
# Either use the LOCAL_USER_ID if passed in at runtime or
# fallback

if [ -z "${LOCAL_USER_ID}" ]
then
    echo "No local user id passed in, so just using the existing one"
else
    usermod -u ${LOCAL_USER_ID} author
    groupmod -g ${LOCAL_USER_ID} author
fi

gosu author "$@"
