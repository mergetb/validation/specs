# MergeTB Specs

This repository contains the specifications for the MergeTB platform. This
includes

- [Experimentation](experimentation.adoc): Specification of functionalities
  available to experimenters using MergeTB as a research platform.

- [Commissioning](commissioning.adoc): Specification of functionalities
  available to resource providers using MergeTB to contribute resources.

- [Portal Administration](portal-admin.adoc): Specification of functionalities
  available to portal administrators.

- [Facility Administration](facility-admin.adoc): Specification of
  functionalities available to testbed facility administrators.

- [Facility](facility.adoc): Specification of functionalities provided
  by Merge testbed facilities.

## Releases

Spec releases are built by this repository's CI process.

### Master

- [Experimentation](https://gitlab.com/mergetb/validation/specs/-/jobs/artifacts/master/file/experimentation.html?job=build)

- [Facility](https://gitlab.com/mergetb/validation/specs/-/jobs/artifacts/master/file/facility.html?job=build)

## Development

There are several targets in the Makefile for running "local" builds of the documents and the JSON indices.  The local builds
are run within a Docker container designed to match the pattern used by the CI process.  When in doubt, the CI process takes
precedence as the canonical way to build, and the local build recipe should be kept aligned with that.

Example workflow for a local build:

1. Edit in the "src" for the docs in your favorite editor
2. Run these Makefile targets to build locally and verify manually
   ```
   cd <repo>
   make docker_build docker_run
   ```
3. The build recipe will mount your repo's directory, and emit the build output there.  It will attempt to match file permissions to
   your current local user as well.

Caveats:

* You need to have the following setup locally
  * Docker
  * Your local user must be in the docker group
  * You must run docker as the local user
