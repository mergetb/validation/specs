specs = experimentation \
		facility

.PHONY: all
all: docs coverage

.PHONY: docs
docs: $(addsuffix .html,$(specs))

.PHONY: coverage
coverage: $(addsuffix .json,$(specs))

%.html: %.adoc
	asciidoctor $<

%.json: %.adoc
	./sections.rb $< > $@

.PHONY: clean
clean:
	rm -f *.html *.json

#
# Local builds via docker
#

.PHONY: docker_clean docker_build docker_debug 

LOCAL_USER_ID = $(shell id -u)

CUSTOM_DOCKER_IMAGE_TAG = mergetb-validation-specs
CUSTOM_DOCKER_CONTAINER_NAME = local-$(CUSTOM_DOCKER_IMAGE_TAG)

DOCKER_RUN_OPTS = --env LOCAL_USER_ID=$(LOCAL_USER_ID)
DOCKER_RUN_OPTS += -v $(shell pwd):/home/author/docs/

docker_clean:
	-docker rmi -f $(CUSTOM_DOCKER_IMAGE_TAG)

docker_build:
	docker build -t $(CUSTOM_DOCKER_IMAGE_TAG) .

docker_run:
	docker run $(DOCKER_RUN_OPTS) $(CUSTOM_DOCKER_IMAGE_TAG) make

docker_debug: docker_build
	docker run -it $(DOCKER_RUN_OPTS) $(CUSTOM_DOCKER_IMAGE_TAG) /bin/bash
