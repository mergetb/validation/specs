FROM debian:bullseye

#
# Install packages
#

RUN apt update && \
    apt install -y \
    asciidoctor \
    gosu \
    make \
    ruby; \
    rm -rf /var/lib/apt/lists/*

#
# Create non-root user
#

RUN addgroup --gid 1000 --system author
RUN adduser --home /home/author --shell /bin/bash --uid 1000 --gid 1000 --system author

#
# Copy over entrypoint.sh script
#

RUN mkdir /opt/app
COPY docker/entrypoint.sh /opt/app/entrypoint.sh
RUN chmod u+x /opt/app/entrypoint.sh

VOLUME ["/home/author/docs"]
WORKDIR /home/author/docs
ENTRYPOINT ["/opt/app/entrypoint.sh"]
